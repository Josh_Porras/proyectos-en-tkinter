from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import winsound #agregar sonido
import time;
import random
import datetime


class Hospital():

    def __init__(self,root):
        self.d_date = datetime.datetime.now()
        self.reg_format_date = self.d_date.strftime("%I:%M:%S %p")

        self.root = root
        self.root.title('HOSPITAL MATASANOS')
        self.root.geometry('1350x750+00+0')
        self.root.iconbitmap('deth.ico')#icono
        #self.root.configure(bg='light green')
        
#=======================variables==============================================================#
        self.tabla_nombre = StringVar() #TABLA DE NOMBRES DE MEDICAMENTOS
        self.Ref = StringVar() #No. de referancias
        self.Dose = StringVar()#DOSIS
        self.tabla_numero = StringVar()#NUMERO DE PASTILLAS (TABLETAS)
        self.Lot = StringVar() #NUMERO DE LOTES
        self.IssuedData = StringVar() #FEHAC DE EMISION
        self.ExpDate = StringVar() #FECHA DE EXPIRACION
        self.DailyDose = StringVar() #DOSIS DIARIA
        self.PossibleSideEffeects = StringVar()
        self.FurtherInformation = StringVar() #MAS INFORMACION
        self.StrongAdvice = StringVar() #ALMACENAMINETO
        self.DrivingUsingMachines = StringVar()#MAQUINNA DE CONDUCCION (CONDUCCION)
        self.HowtoUseingMachines = StringVar()
        self.PatientId = StringVar() #NUMERO ID DE PASIENTE
        self.PatientNHSNo = StringVar()
        self.PatientName = StringVar() #NOMBRE DE PACIENTE
        self.DateofBirth = StringVar() #FECHA DE NACIMIENTO
        self.PatientAddress = StringVar() #DIRECCION
#===================FRAME_PRINCIPALES=======================================================#
        self.Principal_Frame = Frame(self.root)
        self.Principal_Frame.grid()

        self.frame_1 = Frame(self.Principal_Frame,width=1350,padx=20,relief='raised')#aqui esta contenido el label del titulo
        self.frame_1.pack(side=TOP)

        self.Lb_titulo = Label(self.frame_1,font=('arial',20,'bold'),text='HOSPITAL MATASANOS',padx=20)#Label del titulo 
        self.Lb_titulo.grid(row=0,column=0)
#===================FRAME_SECUNDARIOS========================================================#
        self.Info_frame = Frame(self.Principal_Frame,padx=20,width=1350,height=400,bd=10,relief='ridge',bg='powder blue')#frame de las descripciones abajo
        self.Info_frame.pack(side=BOTTOM)

        self.botonera_f = Frame(self.Principal_Frame,padx=20,width=1350,height=70,bd=10,relief='ridge',bg='powder blue')#frame de los botones(botonera)
        self.botonera_f.configure(cursor="plus")#cambio de imagen del cursor
        self.botonera_f.pack(side=BOTTOM)

        self.Datos_frame = Frame(self.Principal_Frame,padx=20,width=1350,height=400,bd=10,relief='ridge',bg='gray')#frame sub-principal
        self.Datos_frame.pack(side=BOTTOM)

        self.DatosL_frame = Frame(self.Datos_frame,padx=14,width=900,height=400,bd=10,relief='ridge',bg='powder blue')#frame que contiene los labels y entry's
        self.DatosL_frame.pack(side=LEFT)

        self.DatoR_frame = Frame(self.Datos_frame,padx=8,width=450,height=400,bd=10,relief='ridge',bg='powder blue')#frame de las descripciones derecha
        self.DatoR_frame.pack(side=RIGHT)
        
#=======================TABLAS_Y_NOMBRES========================================================================================#
        #Nomb. Medicamento //combobox
        self.Lb0_ = Label(self.DatosL_frame,font=('arial',10,'bold'),text='NOMBRE  MEDICAMENTOS',bg='powder blue')
        self.Lb0_.grid(row=0,column=0,sticky=W)

        self.tabla_nombre = ttk.Combobox(self.DatosL_frame,textvariable=self.tabla_nombre,state='readonly',font=('arial',12,'bold'),width=20)
        self.tabla_nombre['value']=('','Paracetamol','Vic','Abortivos','Racumin','Cloro','La ultima cena','Anavolicos','Manzana curativa','Perico','Foco','SS')
        self.tabla_nombre.current(0)
        self.tabla_nombre.grid(row=0,column=1)

        #mas informacion
        self.Lb1 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='MAS INFORMACION',bg='powder blue')
        self.Lb1.grid(row=0,column=2,sticky=W)
        self.En1 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.FurtherInformation)
        self.En1.grid(row=0,column=3)

        #referencias
        self.Lb2 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='NO. REFRERENCIA',bg='powder blue')
        self.Lb2.grid(row=1,column=0,sticky=W)
        self.En2 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.Ref)
        self.En2.grid(row=1,column=1)

        #almacenamiento
        self.Lb3 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='ALMACENAMIENTO',bg='powder blue')
        self.Lb3.grid(row=1,column=2,sticky=W)
        self.En3 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.StrongAdvice)
        self.En3.grid(row=1,column=3)

        #DOSIS
        self.Lb4 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='DOSIS',bg='powder blue')
        self.Lb4.grid(row=2,column=0,sticky=W)
        self.En4 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.Dose)
        self.En4.grid(row=2,column=1)

        #CONDUCCION
        self.Lb5 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='CONDUCCION',bg='powder blue')
        self.Lb5.grid(row=2,column=2,sticky=W)
        self.En5 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.DrivingUsingMachines)
        self.En5.grid(row=2,column=3)

        #NO. TALBETAS
        self.Lb6 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='NUMERO DE TABLETAS',bg='powder blue')
        self.Lb6.grid(row=3,column=0,sticky=W)
        self.En6 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.tabla_numero)
        self.En6.grid(row=3,column=1)

        #CONDUCCION #2
        self.Lb7 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='CONDUCCION',bg='powder blue')
        self.Lb7.grid(row=3,column=2,sticky=W)
        self.En7 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.DrivingUsingMachines)
        self.En7.grid(row=3,column=3)

        #NO DE LOTES
        self.Lb8 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='NO. LOTES',bg='powder blue')
        self.Lb8.grid(row=4,column=0,sticky=W)
        self.En8 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.Lot)
        self.En8.grid(row=4,column=1)

        #NUMERO ID PASIENTE
        self.Lb9 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='ID PASIENTES',bg='powder blue')
        self.Lb9.grid(row=4,column=2,sticky=W)
        self.En9 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.PatientId)
        self.En9.grid(row=4,column=3)

        #FECHA DE EMISIION
        self.Lb10 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='FECHA DE EMISION',bg='powder blue')
        self.Lb10.grid(row=5,column=0,sticky=W)
        self.En10 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.IssuedData)
        self.En10.grid(row=5,column=1)

        #FECHA DE EXPIRACION
        self.Lb10 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='FECHA DE EXPIRACION',bg='powder blue')
        self.Lb10.grid(row=5,column=2,sticky=W)
        self.En10 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.ExpDate)
        self.En10.grid(row=5,column=3)

        #DOSIS DIARIA
        self.Lb11 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='DOSIS DIARIA',bg='powder blue')
        self.Lb11.grid(row=6,column=0,sticky=W)
        self.En11 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.DailyDose)
        self.En11.grid(row=6,column=1)

        #NOMBRE DE PACIENTE
        self.Lb12 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='NOMBRE DE PACIENTE',bg='powder blue')
        self.Lb12.grid(row=6,column=2,sticky=W)
        self.En12 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.PatientName)
        self.En12.grid(row=6,column=3)

        #FECHA DE NACIMIENTO
        self.Lb12 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='FECHA DE NACIMIENTO',bg='powder blue')
        self.Lb12.grid(row=7,column=0,sticky=W)
        self.En12 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.DateofBirth)
        self.En12.grid(row=7,column=1)

        #DIRECCION
        self.Lb12 = Label(self.DatosL_frame,font=('arial',10,'bold'),text='DIRECCION',bg='powder blue')
        self.Lb12.grid(row=7,column=2,sticky=W)
        self.En12 = Entry(self.DatosL_frame,font=('arial',10,'bold'),textvariable=self.PatientAddress)
        self.En12.grid(row=7,column=3)


#====================================BOTONES===========================================================================================#
        self.bt_001 = Button(self.botonera_f,font=('arial',12,'bold'),text='Descripcion',width=10,bd=8,relief='raised',command=self.descript)
        self.bt_001.grid(row=0,column=0,padx=10)

        self.bt_002 = Button(self.botonera_f,font=('arial',12,'bold'),text='Receta',width=10,bd=8,relief='raised',command=self.receta_)
        self.bt_002.grid(row=0,column=1,padx=10)

        self.bt_003 = Button(self.botonera_f,font=('arial',12,'bold'),text='Borrar Ventanas',width=20,bd=8,relief='raised',command=self.borrar)
        self.bt_003.grid(row=0,column=2,padx=10)

        self.bt_004 = Button(self.botonera_f,font=('arial',12,'bold'),text='Reiniciar',width=10,bd=8,relief='raised',command=self.reset)
        self.bt_004.grid(row=0,column=3,padx=10)

        self.bt_005 = Button(self.botonera_f,font=('arial',12,'bold'),text='Salir',width=10,bd=8,relief='raised',fg='Dark red',command=self.salir)
        self.bt_005.grid(row=0,column=4,pady=10)
        

#=================================ventanas_descripciones============================================================================================#
        self.caja_texto_lbl = Label(self.DatoR_frame,text='DESCRIPCION :',font=('arial',10,'bold'),bg='powder blue')
        self.caja_texto_lbl.grid(row=0,column=0)
        
        self.caja_texto = Text(self.DatoR_frame,font=('arial',12,'bold'),padx=4,pady=4,width=55,height=10)
        self.caja_texto.grid(row=1,column=0)


        #label encabezados de del Text()
        self.lbl_descripciones = Label(self.Info_frame,font=('arial',10,'bold'),bg='powder blue',pady=10,
                                       text='Nomb. Medicina.\tNo. Referencia.\tDosificacion.\tNo. Tabletas.\tLote.\tFecha Asunto\tFecha de Expiracion.\tDosis Diaria')
        self.lbl_descripciones.grid(row=0,column=0)
        
        self.caja_texinfo = Text(self.Info_frame,font=('arial',12,'bold'),padx=5,pady=5,width=100,height=10)
        self.caja_texinfo.grid(row=1,column=0)

#================================================Funciones==========================================================================================#

    def salir(self):
        messagebox.showinfo(title="SALIR",message='Confirmar Salida')
        self.root.destroy()

    def descript(self):
        self.caja_texto.insert(END,"Nombre Pasiente:\t\t"     + self.PatientName.get()+"\n")
        self.caja_texto.insert(END,"Nombre Medicamento :\t\t" + self.tabla_nombre.get()+"\n")
        self.caja_texto.insert(END,"N0.Referencia:\t\t"       + self.Ref.get()+"\n")
        self.caja_texto.insert(END,"Dosis :\t\t"              + self.DailyDose.get()+"\n")
        self.caja_texto.insert(END,"NO.Tabletas:\t\t"         + self.tabla_numero.get()+"\n")
        self.caja_texto.insert(END,"Fecha Actual :\t\t"       + self.reg_format_date+"\n")
        self.caja_texto.insert(END,"Fecha de Expiracion :\t\t"+ self.ExpDate.get()+"\n")
    
    def receta_(self):
        #???
        self.caja_texinfo.insert(END,self.tabla_nombre.get()+"\t\t"+self.Ref.get()+"\t\t"+self.Dose.get()+"\t"+self.tabla_numero.get()+"\t\t"+self.Lot.get()+"\t"+
                                 self.IssuedData.get()+"\t\t"+self.DailyDose.get()+"\t"+self.DailyDose.get())
        #????
    
    def borrar(self):
        messagebox.showinfo(title="Borrar",message="\tLISTO \nVentanas Borradas")
        self.caja_texto.delete("1.0",END)
        self.caja_texinfo.delete("1.0",END)
        
    def reset(self):
        messagebox.showinfo(title="Reiniciar",message="LISTO")
        self.tabla_nombre.set(" ")
        self.Ref.set(" ") 
        self.Dose.set(" ")
        self.tabla_numero.set(" ")
        self.Lot.set(" ")
        self.IssuedData.set(" ") 
        self.ExpDate.set(" ") 
        self.DailyDose.set(" ")
        self.PossibleSideEffeects.set(" ")
        self.FurtherInformation.set(" ") 
        self.StrongAdvice.set(" ")
        self.DrivingUsingMachines.set(" ")
        #self.HowtoUseingMachines.set(" ")
        self.PatientId.set(" ")
        #self.PatientNHSNo.set(" ")
        self.PatientName.set(" ")
        self.DateofBirth.set(" ")
        self.PatientAddress.set(" ") 
        self.caja_texto.delete("1.0",END)
        self.caja_texinfo.delete("1.0",END)
       

def main():
    root = Tk()
    app = Hospital(root)

if __name__ == '__main__':
    main()
      

