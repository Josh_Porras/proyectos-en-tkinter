from tkinter import *
from tkinter import ttk,messagebox
from urllib.request import urlopen
import psutil
import getpass
import json
import socket



class info_sistema():

    def __init__(self,root):
        self.root = root
        self.root.title('INFORMACION DEL SISTEMA V2.0')
        self.root.configure(bg='black')
        root.state('zoomed')#inicializar con maximizacion de ventana
#=======================frames===================================================================
        self.titulo_frame = Frame(self.root,width=900,height=80)
        self.titulo_frame.pack(side=TOP)

        self.txt_frame = Frame(self.root,width=800,height=500)
        self.txt_frame.pack(side=TOP,pady=20)

        self.btn_frame = Frame(self.root,width=500,height=50,bg='black')
        self.btn_frame.pack(side=TOP,pady=10)
#=================================================================================================
        #label titulo
        self.lbl_titulo = Label(self.titulo_frame,font=('arial',20,'bold'),text='INFORMACION DEL SISTEMA',fg='white',bg='black')
        self.lbl_titulo.grid(row=0,column=0)

        #ventana de salida
        self.text_ventana = Text(self.txt_frame,font=('arial',9,'bold'),fg='#00FE1E',bg='#1E1B30')
        self.text_ventana.pack()

        #botones
        self.bt01_cpu = Button(self.btn_frame,font=('arial',13,'bold'),text='CPU',bd=4,relief='raised',bg='black',fg='white',command=self.cpu)
        self.bt01_cpu.pack(side=LEFT,padx=4)

        self.bt02_disco = Button(self.btn_frame,font=('arial',13,'bold'),text='DISCO',bd=4,relief='raised',bg='black',fg='white',command=self.disco)
        self.bt02_disco.pack(side=LEFT,padx=4)

        self.bt03_red = Button(self.btn_frame,font=('arial',13,'bold'),text='RED',bd=4,relief='raised',bg='black',fg='white',command=self.red)
        self.bt03_red.pack(side=LEFT,padx=4)

        self.bt04_info = Button(self.btn_frame,font=('arial',13,'bold'),text='INFORMACION GENERAL',bd=4,relief='raised',bg='black',fg='white',command=self.info_us)
        self.bt04_info.pack(side=LEFT,padx=4)

        self.bt05_limp = Button(self.btn_frame,font=('arial',13,'bold'),text='LIMPIAR',bd=4,relief='raised',bg='black',fg='white',command=self.limpiar)
        self.bt05_limp.pack(side=LEFT,padx=4)

        self.bt06_salir = Button(self.btn_frame,font=('arial',13,'bold'),text='SALIR',bd=4,relief='raised',bg='black',fg='white',command=self.salir)
        self.bt06_salir.pack(side=LEFT,padx=4)
        
        
#=============================FUNCIONES============================================================================
    def salir(self):
        self.root.destroy()

    def limpiar(self):
        self.text_ventana.delete("1.0",END)

    def cpu(self):
         self.text_ventana.insert(END,'\nCPU \t----------------------------------------------------------------------------------')
         self.text_ventana.insert(END,'\n')
         self.text_ventana.insert(END,'\nTiempos de la CPU :')
         self.text_ventana.insert(END,'\n')
         self.text_ventana.insert(END,psutil.cpu_times())
         self.text_ventana.insert(END,'\n\nProcesos CPU :')
         self.text_ventana.insert(END,'\n')
         for x in range(3):
             psutil.cpu_percent(interval=1, percpu=True)
         self.text_ventana.insert(END,x)
         self.text_ventana.insert(END,'\n\n')
         
    def disco(self):
        self.text_ventana.insert(END,'\nDISCO \t-----------------------------------------------------------------------------------')
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'PARTICIONES :')
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,psutil.disk_partitions())
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'ESPACIO DISCO :')
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,psutil.disk_usage('/'))
        self.text_ventana.insert(END,'\n\n')
        
    def red(self):
        self.text_ventana.insert(END,'\nRED \t-----------------------------------------------------------------------------------')
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'PARTICIONES :')
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,psutil.net_io_counters(pernic=True))
        self.text_ventana.insert(END,'\n\n')

    def info_us(self):
        username = getpass.getuser()
        hostname = socket.gethostname()
        maquinaIP = socket.gethostbyname(hostname)

        url = 'http://ipinfo.io/json'#redireccionando
        respuesta = urlopen(url)#accediendo
        data = json.load(respuesta)#leyendo datos
        
        self.text_ventana.insert(END,'\nINFORMACION \t----------------------------------------------------------------------------------')
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'USUARIO       :'+username)
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'HOST              :'+hostname)
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'IP PUBLICA    :'+maquinaIP)
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'IP PRIVADA    :'+data['ip'])
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'PAIS                  :'+data['country'])
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'REGION                :'+data['region'])
        self.text_ventana.insert(END,'\n')
        self.text_ventana.insert(END,'GEOLOCACION           :'+data['loc'])
        self.text_ventana.insert(END,'\n\n')

          
         
         

        



def main():
    root = Tk()
    app = info_sistema(root)


if __name__ == '__main__':
    main()
